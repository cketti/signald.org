---
title: Libraries
---

These are libraries to make interaction with signald easier. [Submit a merge request to add to this list](https://gitlab.com/signald/signald.org/-/blob/main/content/articles/libraries.md)

# Generated

these libraries make use of signald's [protocol documentation](/articles/protocol/documentation/) to generate their core. This ensures they will
support a full range of signald functionality, and naming will be consistant with signald of the documentation:

* [signald-go](https://gitlab.com/signald/signald-go) (golang)
* [aiosignald](https://gitlab.com/nicocool84/aiosignald) (python)
* [kotlin-signald](https://github.com/inthewaves/kotlin-signald) (Kotlin Multiplatform, for JVM and Linux x64)

# Other

* [pysignald](https://pypi.org/project/pysignald/) (python)
* [Semaphore](https://github.com/lwesterhof/semaphore) (python) - a simple (rule-based) bot library
